import numpy as np
import pygame
import sys
import time

board_shape = (20, 20)
board = np.zeros(board_shape)
rng = np.random.default_rng()
board = rng.integers(0, 2, size=(board_shape[0]+2, board_shape[1]+2), dtype=int)
#for ii in range(25, 75):
#    for jj in range(25, 75):
#        board[ii, jj] = 1


def neighbors(board, board_shape, point):
    number_of_neighbours = 0
    i = point[0]
    j = point[1]
    i_left = i - 1
    i_right = i + 2
    j_left = j - 1
    j_right = j + 2
    if (i == 0):
        i_left = 0
    if (i == board_shape[0] - 1):
        i_right = board_shape[0]
    if (j == 0):
        j_left = 0
    if (j == board_shape[1] - 1):
        i_right = board_shape[1]
    for ii in range(i_left, i_right):
        for jj in range(j_left, j_right):
            if(board[ii, jj] == 1):
                number_of_neighbours += 1
    if (board[i, j] == 1):
        number_of_neighbours -= 1
    return number_of_neighbours

def neighbourhood_map(board, board_shape):
    map = np.zeros(board_shape)
    for ii in range(board_shape[0] - 1):
        for jj in range(board_shape[1] - 1):
            map[ii, jj] = neighbors(board, board_shape, [ii, jj])
    return map

def draw_board(board, board_shape, screen):
    rectangles = []
    w, h = screen.get_size()
    width = w / board_shape[0]
    height = h / board_shape[1]
    for ii in range(board_shape[0] - 1):
        for jj in range(board_shape[1] - 1):
            rec = pygame.Rect(ii*width, jj*height, width, height)
            colour = (0, 0, 0)
            if (board[ii, jj] == 1):
                colour = (255, 255, 255)
            pygame.draw.rect(screen, colour, rec)

def new_board(board_old, board_shape):
    new = board_old.copy()
    neighbourhood = neighbourhood_map(board, board_shape)
    for ii in range(board_shape[0] - 1):
        for jj in range(board_shape[1] - 1):
            if (board_old[ii, jj] == 1):
                if ((neighbourhood[ii, jj] < 2) or (neighbourhood[ii, jj] > 3)):
                    new[ii, jj] = 0
            else:
                if (neighbourhood[ii, jj] == 3):
                    new[ii, jj] = 1
    return new


pygame.init()
screen = pygame.display.set_mode((600, 600))
draw_board(board, board_shape, screen)

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            sys.exit()
    pygame.display.flip()
    time.sleep(1)
    board = new_board(board, board_shape)
    draw_board(board, board_shape, screen)


